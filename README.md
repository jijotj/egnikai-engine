#Egnikai Engine

Egnikai engine is a microservice designed to execute long running tasks such as cloning, saving, and running.

##Tech Stack
* Springboot
* Rabbit-MQ

## How to Run

***NOTE*** Please start the DB container first

###Configuration before running

####Configure git 
create a private ssh key at sshKey/egnikai

Add the git repository host key to sshKey/known_hosts

Configure the git repository to use ssh authentication

####Configure rabbit mq 
setup password for the rabbit mq  using the below command
```bash
pass add egnikai/rmq
```

####Start the application

```bash
bin/start-rmq.sh
bin/build.sh .
bin/start-engine.sh .
```

