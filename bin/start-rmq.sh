#!/usr/bin/env bash

EGNIKAI_RABBIT_MQ_CONTAINER_NAME="egnikaimq"
EGNIKAI_RABBIT_MQ_HOSTNAME="egnikaimq"
EGNIKAI_RABBIT_MQ_USERID="egnikaimq"
EGNIKAI_RABBIT_MQ_PASSWORD=`pass egnikai/rmq`
EGNIKAI_RABBIT_MQ_MGMT_PORT=8119

echo "Stopping existing containers..."
sudo docker stop ${EGNIKAI_RABBIT_MQ_CONTAINER_NAME}

echo "Removing containers..."
sudo docker rm ${EGNIKAI_RABBIT_MQ_CONTAINER_NAME}

echo "Starting Egnikai Rabbit MQ..."
sudo docker run -d --hostname ${EGNIKAI_RABBIT_MQ_HOSTNAME} --name ${EGNIKAI_RABBIT_MQ_CONTAINER_NAME} -p ${EGNIKAI_RABBIT_MQ_MGMT_PORT}:15672 -p 5672:5672 -e RABBITMQ_DEFAULT_USER=${EGNIKAI_RABBIT_MQ_USERID} -e RABBITMQ_DEFAULT_PASS=${EGNIKAI_RABBIT_MQ_PASSWORD} rabbitmq:3-management
