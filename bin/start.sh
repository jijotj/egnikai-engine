#!/usr/bin/env bash

# DO NOT USE THIS FILE LOCALLY. ONLY MEANT FOR DOCKER CONTAINER

base=$1
echo "Using $base as base"
echo "setting up..."
APPJAR=egnikai-engine-0.0.1-SNAPSHOT.jar

java -jar $base/$APPJAR --spring.rabbitmq.password=${EGNIKAI_RABBIT_MQ_PASSWORD} --spring.datasource.password=${EGNIKAI_DB_PASSWORD} --egnikai.engine.ssh.key_path=${EGNIKAI_SSH_KEY_PATH}  --spring.datasource.url=${EGNIKAI_DS_URL} --spring.rabbitmq.host=${RMQ_SERVICE_SERVICE_HOST}