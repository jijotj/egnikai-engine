package com.thoughtworks.in.egnikaiengine.controller;

import com.thoughtworks.in.egnikaiengine.dto.CloningMessageDTO;
import com.thoughtworks.in.egnikaiengine.service.CandidateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CloningQueueMessageReceiver {

    private static final Logger logger = LoggerFactory.getLogger(CloningQueueMessageReceiver.class);

    private CandidateService candidateService;

    @Autowired
    public CloningQueueMessageReceiver(CandidateService candidateService) {
        this.candidateService = candidateService;
    }

    @RabbitListener(queues = "${egnikai.engine.queue.cloning}", containerFactory = "jsaFactory")
    public void receivedMessage(CloningMessageDTO cloningMessageDTO) {

        try {
            logger.info("clone operation with ID " + cloningMessageDTO.getOperationId() + " started for candidate " + cloningMessageDTO.getCandidateId());
            candidateService.setupEnvironmentForCandidate(cloningMessageDTO);
            logger.info("clone operation with ID " + cloningMessageDTO.getOperationId() + " completed for candidate " + cloningMessageDTO.getCandidateId());
        } catch (Exception exception) {
            logger.error("clone operation failed for " + cloningMessageDTO + "\n with exception"+ exception);
        }
    }

}
