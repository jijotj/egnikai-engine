package com.thoughtworks.in.egnikaiengine.persistence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


@Entity
@EqualsAndHashCode
@ToString
public class Candidate implements Serializable {


    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long candidateId;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "User_id")
    private User user;

    @Getter
    @Setter
    private String candidateGitBranch;

    @Getter
    @Setter
    private Date challengeDate;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Challenge_id")
    private Challenge challenge;

    @Getter
    @Setter
    private boolean isPassed;

    @Getter
    @Setter
    private int score;

    @Getter
    @Setter
    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    //@IndexColumn(name="attempted_problems_attempted_problem_id")
    private List<AttemptedProblem> attemptedProblems;

    @Getter
    @CreationTimestamp
    private Timestamp createdAt;

    @Getter
    @UpdateTimestamp
    private Timestamp updatedDateTime;
//    @Getter
//    @Setter
//    @OneToMany(fetch = FetchType.LAZY)
//    private List<Operation> operations;

}
