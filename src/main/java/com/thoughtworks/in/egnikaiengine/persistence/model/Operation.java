package com.thoughtworks.in.egnikaiengine.persistence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@EqualsAndHashCode
public class Operation implements Serializable {
    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long operationId;

    @Getter
    @Setter
    private String operationName;

    @Getter
    @Setter
    private String operationStatus;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @JoinColumn(name = "Candidate_id")
    //@IndexColumn(name="CANDIDATE_IDX")
    private Candidate candidate;

    @Getter
    @Setter
    private String errorInformation;

    @Getter
    @Setter
    @UpdateTimestamp
    private Timestamp updatedDateTime;


    @Getter
    @Setter
    @CreationTimestamp
    private Timestamp createdDateTime;
}
