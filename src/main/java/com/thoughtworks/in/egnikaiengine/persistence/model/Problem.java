package com.thoughtworks.in.egnikaiengine.persistence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@EqualsAndHashCode
@ToString
public class Problem {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long problemId;

    /**
     * Every problem must have a unique title
     */
    @Getter
    @Setter
    private String title;

    /**
     * Problem statement
     */
    @Getter
    @Setter
    @Lob
    private String description;

    /**
     * If this problem is optional to attend
     */
    @Getter
    @Setter
    private boolean optional;

    /**
     * points to be credited if this problem is solved, i.e., all testCases passed.
     */
    @Getter
    @Setter
    private int credit;

    /**
     * List of all test cases in the problem
     */
    @Getter
    @Setter
    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<TestCase> testCases;

    /**
     * List of all source entities associated with this problem
     */
    @Getter
    @Setter
    @OneToMany(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<SourceCode> sourceCodes;


}
