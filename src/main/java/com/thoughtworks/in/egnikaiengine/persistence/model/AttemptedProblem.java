package com.thoughtworks.in.egnikaiengine.persistence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Entity
@EqualsAndHashCode
@ToString
public class AttemptedProblem {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long attemptedProblemId;

    /**
     * linked problem
     */
    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER)
    private Problem problem;

    @Getter
    @Setter
    private boolean isPassed;

    /**
     * score of the problem
     */
    @Getter
    @Setter
    private int score;
}
