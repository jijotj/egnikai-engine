package com.thoughtworks.in.egnikaiengine.persistence.model;

public enum OperationStatus {
    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED,
    FAILED,
    SYSTEM_FAILURE
}
