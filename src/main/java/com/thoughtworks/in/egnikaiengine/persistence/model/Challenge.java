package com.thoughtworks.in.egnikaiengine.persistence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@EqualsAndHashCode
@ToString
public class Challenge implements Serializable {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long challengeId;

    /**
     * Challenge header, tips etc
     */
    @Getter
    @Setter
    private String challengeTitle;

    /**
     * Challenge header, tips etc
     */
    @Getter
    @Setter
    @Lob
    private String description;

    /**
     * Challenge platform name
     */
    @Getter
    @Setter
    private String platformName;

    /**
     * Challenge Git URL
     */
    @Getter
    @Setter
    private String challengeGitURL;

    /**
     * List of Problems in this challenge
     */
    @Getter
    @Setter
    @OneToMany(fetch = FetchType.EAGER)
    private List<Problem> problems;

}
