package com.thoughtworks.in.egnikaiengine.persistence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;


/**
 * This class represents a test which can be either public or private
 * private testCases are not displayed to the user
 */
@Entity(name = "Test")
@EqualsAndHashCode
@ToString
public class TestCase implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private long testId;

    /**
     * TestCase File name
     */
    @Getter
    @Setter
    private String testFileName;

    /**
     * TestCase class name
     */
    @Getter
    @Setter
    private String className;

    /**
     * TestCase Type, as of now PUBLIC or PRIVATE
     */
    @Getter
    @Setter
    private TestType type;

    /**
     * Relative path to the test case file
     */
    @Getter
    @Setter
    private String filePath;


    public TestCase() {

    }


}
