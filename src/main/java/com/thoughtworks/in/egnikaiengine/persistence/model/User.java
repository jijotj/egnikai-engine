package com.thoughtworks.in.egnikaiengine.persistence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@EqualsAndHashCode
@ToString
public class User implements Serializable {

    @Getter
    @Setter
    @Id
    protected String userId;

    @Getter
    @Setter
    protected String userName;

    @Getter
    @Setter
    protected String password;

    @Getter
    @Setter
    protected String role;

    @Getter
    @Setter
    protected boolean enabled;

}
