package com.thoughtworks.in.egnikaiengine.persistence.repository;

import com.thoughtworks.in.egnikaiengine.persistence.model.Problem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProblemRepository extends CrudRepository<Problem, Long> {
    Problem findByProblemId(long problemId);
}
