package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.dto.CloningMessageDTO;
import com.thoughtworks.in.egnikaiengine.dto.SavingMessageDTO;
import com.thoughtworks.in.egnikaiengine.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikaiengine.persistence.model.AttemptedProblem;
import com.thoughtworks.in.egnikaiengine.persistence.model.Candidate;
import com.thoughtworks.in.egnikaiengine.persistence.model.Operation;
import com.thoughtworks.in.egnikaiengine.persistence.model.OperationStatus;
import com.thoughtworks.in.egnikaiengine.persistence.repository.CandidateRepository;
import com.thoughtworks.in.egnikaiengine.utils.GitUtils;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CandidateService {

    private static final Logger logger = LoggerFactory.getLogger(CandidateService.class);
    private GitUtils gitUtils;
    private OperationService operationService;
    private CandidateRepository candidateRepository;
    private ProblemService problemService;
    private String codeDirectoryPrefix;
    private String candidatePrefix;
    private String COMMIT_MESSAGE = "save file";


    @Autowired
    public CandidateService(GitUtils gitUtils,
                            OperationService operationService,
                            CandidateRepository candidateRepository,
                            ProblemService problemService,
                            @Value("${egnikai.engine.dir.code.sharedpath}") String codeDirectoryPrefix,
                            @Value("${egnikai.engine.dir.code.candidatePrefix}") String candidatePrefix) {
        this.gitUtils = gitUtils;
        this.operationService = operationService;
        this.candidateRepository = candidateRepository;
        this.problemService = problemService;
        this.codeDirectoryPrefix = codeDirectoryPrefix;
        this.candidatePrefix = candidatePrefix;
    }

    private void cloneGitRepositoryForCandidate(CloningMessageDTO cloningMessageDTO) {
        long candidateId = cloningMessageDTO.getCandidateId();
        String branchName = candidatePrefix + candidateId;
        try {
            gitUtils.cloneAndSwitchTo(cloningMessageDTO.getGitURL(), getCandidateFolder(cloningMessageDTO.getCandidateId()), branchName);

            Optional<Candidate> candidateDAO = candidateRepository.findById(candidateId);
            candidateDAO.ifPresent(candidate -> {
                candidate.setCandidateGitBranch(branchName);
                logger.debug("Updating branch name for candidate ", cloningMessageDTO.getCandidateId(), cloningMessageDTO);
                candidateRepository.save(candidate);
            });
        } catch (GitAPIException | IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public void setupEnvironmentForCandidate(CloningMessageDTO cloningMessageDTO) {
        long operationId = cloningMessageDTO.getOperationId();
        try {
            operationService.writeCloneStatusToDB(operationId, OperationStatus.IN_PROGRESS);
            logger.debug("updated cloning status to in progress with operation id " + operationId +
                    "for candidate" + cloningMessageDTO.getCandidateId());
            cloneGitRepositoryForCandidate(cloningMessageDTO);
            operationService.writeCloneStatusToDB(operationId, OperationStatus.COMPLETED);
            logger.debug("updated cloning operationstatus to completed with operation id" + operationId
                    + "for candidate" + cloningMessageDTO.getCandidateId());
        } catch (Exception e) {
            operationService.writeCloneStatusToDB(operationId, OperationStatus.FAILED);
            logger.error("Cloning operation failed with Operation id ", operationId, " for candidate ",
                    cloningMessageDTO.getCandidateId());
        }
    }

    public void saveProblem(SavingMessageDTO savingMessageDTO) {
        try {
            logger.info("Saving Problem for candidate " + savingMessageDTO.getCandidateId());
            gitUtils.commitAndPush(getCandidateFolder(savingMessageDTO.getCandidateId()), COMMIT_MESSAGE);
        } catch (Exception exception) {
            logger.error("Saving failed for candidate " + savingMessageDTO.getCandidateId(), exception);
        }
    }

    void updateAttemptedProblemInDB(long operationId, long problemId) {
        logger.debug("Updating attempted problem with ", problemId, "operationId ", operationId);
        try {
            Operation operation = operationService.getOperation(operationId);
            Candidate candidate = operation.getCandidate();
            List<AttemptedProblem> attemptedProblemList = candidate.getAttemptedProblems().stream()
                    .filter(predicate -> predicate.getProblem().getProblemId() == problemId).collect(Collectors.toList());
            if (attemptedProblemList.size() > 0) {
                attemptedProblemList.forEach(attemptedProblem -> {
                    setIsPassedStatus(operation, attemptedProblem);
                });
            } else {
                addNewAttemptedProblemToCandidate(problemId, operation, candidate);
            }
            logger.debug("[candidate is]:" + candidate.toString());
            candidateRepository.save(candidate);
        } catch (Exception e) {
            logger.error("updating attempted problems operation for operation Id ", operationId, e,
                    " failed, problem Id ", problemId);
        }
    }

    private void setIsPassedStatus(Operation operation, AttemptedProblem attemptedProblem) {
        if (OperationStatus.COMPLETED.name() == operation.getOperationStatus()) {
            attemptedProblem.setPassed(true);
        } else {
            attemptedProblem.setPassed(false);
        }
    }

    private void addNewAttemptedProblemToCandidate(long problemId, Operation operation, Candidate candidate) throws ResourceNotFoundException {
        logger.debug("new problem attempted ", problemId, operation, "by candidate ", candidate);
        AttemptedProblem attemptedProblem = new AttemptedProblem();
        attemptedProblem.setPassed(OperationStatus.COMPLETED.name().equals(operation.getOperationStatus()));
        attemptedProblem.setProblem(problemService.getProblem(problemId));
        candidate.getAttemptedProblems().add(attemptedProblem);
    }


    File getCandidateFolder(long candidateId) {
        return new File(codeDirectoryPrefix + "/" + candidatePrefix + candidateId);
    }
}
