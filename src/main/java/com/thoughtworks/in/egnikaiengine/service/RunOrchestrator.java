package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.dto.CompileRequestDTO;
import com.thoughtworks.in.egnikaiengine.dto.RunRequestDTO;
import com.thoughtworks.in.egnikaiengine.dto.RunningMessageDTO;
import com.thoughtworks.in.egnikaiengine.persistence.model.OperationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RunOrchestrator {

    private static final Logger logger = LoggerFactory.getLogger(RunOrchestrator.class);

    private CompilerService compilerService;
    private RunnerService runnerService;
    private CandidateService candidateService;
    private ScoreService scoreService;
    private OperationService operationService;
    private String srcDir;

    @Autowired
    public RunOrchestrator(CompilerService compilerService,
                           RunnerService runnerService,
                           CandidateService candidateService,
                           ScoreService scoreService,
                           OperationService operationService,
                           @Value("${egnikai.engine.code.src.dir}") String srcDir) {
        this.compilerService = compilerService;
        this.runnerService = runnerService;
        this.candidateService = candidateService;
        this.scoreService = scoreService;
        this.operationService = operationService;
        this.srcDir = srcDir;
    }

    public void compileAndRun(RunningMessageDTO runningMessageDTO) {
        operationService.updateCompileStatusToDB(runningMessageDTO.getOperationId(), OperationStatus.IN_PROGRESS.name(), "");
        CompileRequestDTO compileRequestDTO = new CompileRequestDTO(srcDir, runningMessageDTO.getCandidateId(), runningMessageDTO.getOperationId(), runningMessageDTO.getProblemId());

        logger.debug("Starting compilation of problem ", runningMessageDTO.getProblemId(), " for candidate ",
                runningMessageDTO.getCandidateId());
        int compileStatus = compilerService.compile(compileRequestDTO);
        logger.debug("Compile completed with status " + compileStatus + "problem ", runningMessageDTO.getProblemId(), " for candidate ",
                runningMessageDTO.getCandidateId());
        if (compileStatus == 0) {
            logger.debug("Starting execution of problem ", runningMessageDTO.getProblemId(), " for candidate ",
                    runningMessageDTO.getCandidateId());
            this.executeCode(runningMessageDTO);
            logger.debug("Execution completed problem ", runningMessageDTO.getProblemId(), " for candidate ",
                    runningMessageDTO.getCandidateId());
        }
        candidateService.updateAttemptedProblemInDB(runningMessageDTO.getOperationId(), runningMessageDTO.getProblemId());
        scoreService.calculateScore(runningMessageDTO.getCandidateId());
    }

    private void executeCode(RunningMessageDTO runningMessageDTO) {
        RunRequestDTO runRequestDTO = new RunRequestDTO(runningMessageDTO.getOperationId(), runningMessageDTO.getCandidateId(), runningMessageDTO.getProblemId());
        runnerService.runAllTests(runRequestDTO);
    }

}