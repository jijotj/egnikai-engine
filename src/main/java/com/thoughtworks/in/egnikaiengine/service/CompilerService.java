package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.dto.CompileRequestDTO;
import com.thoughtworks.in.egnikaiengine.dto.ProcessOutput;
import com.thoughtworks.in.egnikaiengine.persistence.model.OperationStatus;
import com.thoughtworks.in.egnikaiengine.utils.ScriptUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompilerService {
    private static final Logger logger = LoggerFactory.getLogger(CompilerService.class);

    private OperationService operationService;

    private ScriptUtils scriptUtils;

    private static String SCRIPT_NAME = "compile_candidate_code";

    @Autowired
    public CompilerService(OperationService operationService,
                           ScriptUtils scriptUtils) {
        this.operationService = operationService;
        this.scriptUtils = scriptUtils;
    }

    public int compile(CompileRequestDTO compileRequestDTO) {
        int exitStatus = 1;
        try {

            int timeoutInMillis = 20000;
                ProcessOutput compileProcessOutput =scriptUtils.runScript(SCRIPT_NAME, timeoutInMillis,
                    String.valueOf(compileRequestDTO.getCandidateId()), String.valueOf(compileRequestDTO.getProblemId()));

            exitStatus = compileProcessOutput.getExitStatus();

            logger.debug("Compilation completed for candidate " + compileRequestDTO.getCandidateId() + ", for problem " + compileRequestDTO.getProblemId() + "with status " + exitStatus);

            if (exitStatus != 0) {
                updateCompileFailureStatus(compileRequestDTO, compileProcessOutput.getError());
            }
        } catch (Exception exception) {
            logger.error("Exception while compiling problem : " + compileRequestDTO.getProblemId() + "for candidate" + compileRequestDTO.getCandidateId() , exception);
            operationService.updateCompileStatusToDB(compileRequestDTO.getOperationId(), OperationStatus.SYSTEM_FAILURE.name(), exception.toString());
        }
        return exitStatus;
    }

    private void updateCompileFailureStatus(CompileRequestDTO compileRequestDTO, String errorStream){
        if (errorStream != null && !errorStream.isEmpty()) {
            operationService.updateCompileStatusToDB(compileRequestDTO.getOperationId(), OperationStatus.FAILED.name(), errorStream);
        }
    }

}
