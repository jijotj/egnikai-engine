package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.dto.ProcessOutput;
import com.thoughtworks.in.egnikaiengine.dto.RunRequestDTO;
import com.thoughtworks.in.egnikaiengine.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikaiengine.persistence.model.Operation;
import com.thoughtworks.in.egnikaiengine.persistence.model.OperationStatus;
import com.thoughtworks.in.egnikaiengine.persistence.model.Problem;
import com.thoughtworks.in.egnikaiengine.persistence.model.TestCase;
import com.thoughtworks.in.egnikaiengine.utils.ScriptUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RunnerService {

    private static final Logger logger = LoggerFactory.getLogger(RunnerService.class);
    private OperationService operationService;
    private ScriptUtils scriptUtils;
    private ProblemService problemService;



    @Autowired
    public RunnerService(OperationService operationService,
                         ScriptUtils scriptUtils,
                         ProblemService problemService) {
        this.operationService = operationService;
        this.scriptUtils = scriptUtils;
        this.problemService = problemService;
    }

    Operation runAllTests(RunRequestDTO runRequestDTO) {
        logger.debug("Runner service started, candidate:" + runRequestDTO.getCandidateId() + "ProblemId:" + runRequestDTO.getProblemId() + "OperationId" + runRequestDTO.getOperationId());
        Operation operation;
        try {
            String scriptName = "run_candidate_code";
            int timeoutInMillis = 20000;
            ProcessOutput runProcessOutput = scriptUtils.runScript(scriptName, timeoutInMillis, String.valueOf(runRequestDTO.getCandidateId()),
                    String.valueOf(runRequestDTO.getProblemId()), getTestFileNames(runRequestDTO.getProblemId()));

            int exitStatus = runProcessOutput.getExitStatus();

            String errorString = runProcessOutput.getOutput();

            String operationStatus = ((exitStatus == 0) ? OperationStatus.COMPLETED : OperationStatus.FAILED).toString();
            String result = (exitStatus == 0) ? "" : errorString;

            operation = operationService.updateRunStatusToDB(runRequestDTO.getOperationId(), operationStatus, result);
            logger.debug("Finished executing Problem with Id : " + runRequestDTO.getProblemId() + " for candidate" + runRequestDTO.getCandidateId() + "with exit status" + exitStatus);
        } catch (Exception exception) {
            logger.error("Exception while executing Problem with Id : " + runRequestDTO.getProblemId() + " for candidate " + runRequestDTO.getCandidateId() + exception);
            operation = operationService.updateRunStatusToDB(runRequestDTO.getOperationId(), OperationStatus.SYSTEM_FAILURE.name(), exception.toString());
        }
        return operation;
    }


    private String getTestFileNames(long problemId) throws ResourceNotFoundException {
        Problem problem = problemService.getProblem(problemId);

        Optional<String> testFileNames = problem.getTestCases().stream()
                .map(TestCase::getClassName)
                .reduce((name1, name2) -> name1 + " " + name2);
        return testFileNames.orElse("");
    }

}
