package com.thoughtworks.in.egnikaiengine.dto;

import lombok.Data;

@Data
public class RunningMessageDTO {
    private long candidateId;
    private long problemId;
    private long operationId;
    private String platform;
}
