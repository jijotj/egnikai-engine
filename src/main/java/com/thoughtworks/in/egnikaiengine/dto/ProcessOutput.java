package com.thoughtworks.in.egnikaiengine.dto;

import lombok.Data;

@Data
public class ProcessOutput {
    private final String output;
    private final String error;
    private final int exitStatus;

    private final boolean timedOut;
}

