package com.thoughtworks.in.egnikaiengine.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CompileRequestDTO {
    private String srcRootFolder;
    private long candidateId;
    private long operationId;
    private long problemId;
}
