package com.thoughtworks.in.egnikaiengine.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RunRequestDTO {
    private long operationId;
    private long candidateId;
    private long problemId;
}
