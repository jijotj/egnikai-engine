package com.thoughtworks.in.egnikaiengine.dto;

import lombok.Data;

@Data
public class SavingMessageDTO {
    private long candidateId;

    private String filePath;

    private String tempFilePath;

}
