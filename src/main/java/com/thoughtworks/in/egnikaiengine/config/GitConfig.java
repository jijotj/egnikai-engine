package com.thoughtworks.in.egnikaiengine.config;

import com.thoughtworks.in.egnikaiengine.utils.EgnikaiSshSessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

@Configuration
public class GitConfig {
    @Value("${egnikai.engine.ssh.key_path}")
    private String sshKeyPath;

    @Value("${egnikai.engine.ssh.key_passphrase}")
    private String sshKeyPassphrase;

    @Bean
    public EgnikaiSshSessionFactory getGitCredentials() {
        return new EgnikaiSshSessionFactory(new File(sshKeyPath).toPath(),sshKeyPassphrase);
    }



}
