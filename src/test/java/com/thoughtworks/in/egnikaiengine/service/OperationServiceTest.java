package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.persistence.model.Operation;
import com.thoughtworks.in.egnikaiengine.persistence.model.OperationStatus;
import com.thoughtworks.in.egnikaiengine.persistence.repository.OperationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OperationServiceTest {


    private OperationService operationService;

    @Mock
    private OperationRepository operationRepository;


    @Before
    public void setUp() {
        operationService = new OperationService(operationRepository);
    }

    @Test
    public void shouldWriteRunStatusToDB() {
        Operation operation = new Operation();
        operation.setOperationId(1);
        when(operationRepository.findByOperationId(1)).thenReturn(operation);
        Operation actualResult = operationService.updateRunStatusToDB(1, OperationStatus.COMPLETED.name(), "");

        assertEquals(OperationStatus.COMPLETED.name(),actualResult.getOperationStatus());
    }

    @Test
    public void shouldWriteCompileStatusToDB() {
        Operation operation = new Operation();
        operation.setOperationId(1);
        when(operationRepository.findByOperationId(1)).thenReturn(operation);
        Operation actualResult = operationService.updateCompileStatusToDB(1, OperationStatus.COMPLETED.name(), "");

        assertEquals(OperationStatus.COMPLETED.name(),actualResult.getOperationStatus());
    }

    @Test
    public void shouldWriteCloneStatusToDB(){
        Operation operation = new Operation();
        operation.setOperationId(1);
        when(operationRepository.findByOperationId(1)).thenReturn(operation);

        Operation actualResult = operationService.writeCloneStatusToDB(1,OperationStatus.COMPLETED);

        assertEquals(OperationStatus.COMPLETED.name(),actualResult.getOperationStatus());


    }

    @Test
    public void shouldThrowAnExceptionWhenOperationIsNullDuringClone(){

        when(operationRepository.findByOperationId(1)).thenThrow(new NullPointerException());

        operationService.writeCloneStatusToDB(1,OperationStatus.COMPLETED);


    }
}