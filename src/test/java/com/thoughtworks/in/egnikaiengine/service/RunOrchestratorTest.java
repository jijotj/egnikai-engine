package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.dto.CompileRequestDTO;
import com.thoughtworks.in.egnikaiengine.dto.RunRequestDTO;
import com.thoughtworks.in.egnikaiengine.dto.RunningMessageDTO;
import com.thoughtworks.in.egnikaiengine.persistence.model.OperationStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class RunOrchestratorTest {

    private RunOrchestrator runOrchestrator;

    private  RunningMessageDTO runningMessageDTO;
    private CompileRequestDTO compileRequestDTO;
    private RunRequestDTO runRequestDTO;

    @Mock
    private OperationService operationService;

    @Mock
    private CompilerService compilerService;

    @Mock
    private RunnerService runnerService;

    @Mock
    private CandidateService candidateService;

    @Mock
    private ScoreService scoreService;

    @Before
    public void setUp(){

        runOrchestrator = new RunOrchestrator(compilerService,runnerService, candidateService, scoreService, operationService,"src");


        runningMessageDTO = new RunningMessageDTO();
        runningMessageDTO.setCandidateId(4);
        runningMessageDTO.setOperationId(1);
        runningMessageDTO.setPlatform("JAVA");
        runningMessageDTO.setProblemId(1);

        compileRequestDTO = new CompileRequestDTO("src", runningMessageDTO.getCandidateId(), runningMessageDTO.getOperationId(), runningMessageDTO.getProblemId());

        runRequestDTO = new RunRequestDTO(runningMessageDTO.getOperationId(),runningMessageDTO.getCandidateId(), runningMessageDTO.getProblemId());
    }


    @Test
    public void shouldUpdateOperationStatusToIN_PROGRESS() {
        runOrchestrator.compileAndRun(runningMessageDTO);

        Mockito.verify(operationService).updateCompileStatusToDB(runningMessageDTO.getOperationId(), OperationStatus.IN_PROGRESS.name(), "");

    }

    @Test
    public void shouldInvokeCompileMethod() {
        runOrchestrator.compileAndRun(runningMessageDTO);

        Mockito.verify(compilerService).compile(compileRequestDTO);
    }

    @Test
    public void shouldInvokeRunAllTestsMethodWhenCompilationSuccess() {

        when(compilerService.compile(compileRequestDTO)).thenReturn(0);

        runOrchestrator.compileAndRun(runningMessageDTO);

        Mockito.verify(runnerService).runAllTests(runRequestDTO);
    }


    @Test
    public void shouldUpdateAttemptedProblemsInDB() {
        runOrchestrator.compileAndRun(runningMessageDTO);

        Mockito.verify(candidateService).updateAttemptedProblemInDB(runningMessageDTO.getOperationId(), runningMessageDTO.getProblemId());
    }

    @Test
    public void shouldCalculateScoreForGivenCandidate() {
        runOrchestrator.compileAndRun(runningMessageDTO);

        Mockito.verify(scoreService).calculateScore(runningMessageDTO.getCandidateId());
    }
}