package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.persistence.model.AttemptedProblem;
import com.thoughtworks.in.egnikaiengine.persistence.model.Candidate;
import com.thoughtworks.in.egnikaiengine.persistence.model.Problem;
import com.thoughtworks.in.egnikaiengine.persistence.repository.CandidateRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScoreServiceTest {

    private ScoreService scoreService;

    @Mock
    private CandidateRepository candidateRepository;

    @Before
    public void setUp() {
        scoreService = new ScoreService(candidateRepository);
    }

    @Test
    public void shouldCalculateScoreOfAGivenCandidate() {

        Candidate candidate = getCandidate();
        when(candidateRepository.findByCandidateId(1)).thenReturn(candidate);

        scoreService.calculateScore(1);

        assertEquals(10, candidate.getScore());

    }


    @Test
    public void shouldCalculateScoreOfAGivenCandidate1() {

        Candidate candidate = getAnotherCandidateWithTwoSolvedProblems();
        when(candidateRepository.findByCandidateId(1)).thenReturn(candidate);

        scoreService.calculateScore(1);

        assertEquals(20, candidate.getScore());

    }





    private Candidate getCandidate(){
        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);
        candidate.setAttemptedProblems(Arrays.asList(getAttemptedProblem()));

        return candidate;
    }

    private Candidate getAnotherCandidateWithTwoSolvedProblems(){
        Candidate candidate = new Candidate();
        candidate.setCandidateId(1);
        candidate.setAttemptedProblems(Arrays.asList(getAttemptedProblem(), getAttemptedProblem()));

        return candidate;
    }


    private AttemptedProblem getAttemptedProblem(){
        Problem problem = new Problem();
        problem.setProblemId(1);
        problem.setCredit(10);

        AttemptedProblem attemptedProblem = new AttemptedProblem();
        attemptedProblem.setPassed(true);
        attemptedProblem.setProblem(problem);

        return attemptedProblem;
    }
}