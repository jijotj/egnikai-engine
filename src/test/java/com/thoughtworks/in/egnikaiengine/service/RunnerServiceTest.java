package com.thoughtworks.in.egnikaiengine.service;

import com.thoughtworks.in.egnikaiengine.dto.RunRequestDTO;
import com.thoughtworks.in.egnikaiengine.exception.ResourceNotFoundException;
import com.thoughtworks.in.egnikaiengine.persistence.model.Problem;
import com.thoughtworks.in.egnikaiengine.persistence.model.TestCase;
import com.thoughtworks.in.egnikaiengine.utils.ScriptUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RunnerServiceTest {

    private RunnerService runnerService;

    @Mock
    private OperationService operationService;

    @Mock
    private ProblemService problemService;

    @Mock
    private ScriptUtils scriptUtils;

    private RunRequestDTO runRequestDTO;

    @Before
    public void setUp() {
        this.runnerService = new RunnerService(operationService,scriptUtils, problemService );
        runRequestDTO = new RunRequestDTO(1, 1, 1);
    }

    @Test
    public void shouldInvokeMethodToWriteMethodToDB() throws ResourceNotFoundException {
        when(problemService.getProblem(1)).thenReturn(getProblem());

        runnerService.runAllTests(runRequestDTO);

        verify(operationService).updateRunStatusToDB(anyLong(), anyString(), any());
    }


    private Problem getProblem(){

        TestCase testCase = new TestCase();
        testCase.setClassName("PrimeCheck");

        Problem problem = new Problem();

        problem.setProblemId(1);
        problem.setTestCases(Arrays.asList(testCase));

        return problem;

    }
}