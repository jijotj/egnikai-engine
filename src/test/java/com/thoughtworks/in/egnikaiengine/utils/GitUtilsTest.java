package com.thoughtworks.in.egnikaiengine.utils;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@RunWith(MockitoJUnitRunner.class)
public class GitUtilsTest {

    private GitUtils gitUtils;

    @Mock
    private SshSessionFactory sshSessionFactory;


    @Before
    public void setUp() {
        gitUtils = new GitUtils(sshSessionFactory);

    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowAnExceptionWhenImproperGitURLIsPassed() throws GitAPIException, IOException {
        File tempFolder = Files.createTempDirectory("p").toFile();
        gitUtils.cloneAndSwitchTo("url",tempFolder,"c");


    }

    @Test(expected = TransportException.class)
    public void shouldThrowAnExceptionWhenWrongOriginIsPassed() throws GitAPIException, IOException {

        File tempFolder = Files.createTempDirectory("p").toFile();
        gitUtils.commitAndPush(tempFolder,"commit");


    }


}