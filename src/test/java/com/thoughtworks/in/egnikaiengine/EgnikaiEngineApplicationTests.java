package com.thoughtworks.in.egnikaiengine;

import com.jcraft.jsch.JSch;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
		properties = {
				"security.basic.enabled=false",
				"management.security.enabled=false",
				"management.health.db.enabled=false",
				"management.health.rabbitmq.enabled=false",
				"management.health.amqp.enabled=false",
				"egnikai.log.path=src/test/resources/log",
				"egnikai.engine.ssh.key_path=~/.ssh",
				"egnikai.engine.topic-exchange=EgnikaiExchange",
				"egnikai.engine.queue.cloning=EgnikaiCloningQueue",
				"egnikai.engine.routing.cloning=egnikai.engine.routing.cloning.*",
				"egnikai.engine.queue.saving=EgnikaiSavingQueue",
				"egnikai.engine.routing.saving=egnikai.engine.routing.saving.*",
				"egnikai.engine.dir.code.sharedpath=/code",
				"egnikai.engine.dir.code.candidatePrefix=c",
				"egnikai.engine.code.classfiles.dir=/target",
		})
public class EgnikaiEngineApplicationTests {
	@Before
	public void initTests() {
		JSch.setConfig("StrictHostKeyChecking", "no");
	}


	@Test
	public void contextLoads() {
	}

}
