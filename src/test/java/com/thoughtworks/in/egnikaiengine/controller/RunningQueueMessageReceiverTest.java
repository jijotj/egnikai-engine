package com.thoughtworks.in.egnikaiengine.controller;

import com.thoughtworks.in.egnikaiengine.dto.RunningMessageDTO;
import com.thoughtworks.in.egnikaiengine.service.RunOrchestrator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RunningQueueMessageReceiverTest {

    @Mock
    private RunOrchestrator runOrchestrator;

    @InjectMocks
    private RunningQueueMessageReceiver runningQueueMessageReceiver;

    @Test

    public void receivedMessage() {
        RunningMessageDTO runningMessageDTO = new RunningMessageDTO();

        runningQueueMessageReceiver.receivedMessage(runningMessageDTO);

        verify(runOrchestrator).compileAndRun(runningMessageDTO);
    }

    @Test
    public void shouldNotThrowAnExceptionWhenDTOIsNull() {
        RunningMessageDTO runningMessageDTO = null;
        runningQueueMessageReceiver.receivedMessage(runningMessageDTO);
    }
}