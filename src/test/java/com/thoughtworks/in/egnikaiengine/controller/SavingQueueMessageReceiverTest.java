package com.thoughtworks.in.egnikaiengine.controller;

import com.thoughtworks.in.egnikaiengine.dto.SavingMessageDTO;
import com.thoughtworks.in.egnikaiengine.service.CandidateService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SavingQueueMessageReceiverTest {

    @Mock
    private CandidateService candidateService;

    @InjectMocks
    private SavingQueueMessageReceiver savingQueueMessageReceiver;

    @Test
    public void shouldCallCandidateService_saveProblemWhenMessageReceived() {

        SavingMessageDTO savingMessageDTO = new SavingMessageDTO();
        savingMessageDTO.setCandidateId(1);
        savingMessageDTO.setFilePath("problem/file/path");
        savingMessageDTO.setTempFilePath("temp/path");

        savingQueueMessageReceiver.receivedMessage(savingMessageDTO);

        Mockito.verify(candidateService).saveProblem(savingMessageDTO);
    }

    @Test
    public void shouldNotThrowAnExceptionWhenDTOIsNull() {
        savingQueueMessageReceiver.receivedMessage(null);
    }
}