#!/usr/bin/env bash
echo "Compiling...$(whoami)"
set -e

CANDIDATE_ID=$1
PROBLEM_ID=$2

CANDIDATE_SRC_DIRECTORY="/code/c${CANDIDATE_ID}"
CANDIDATE_PROBLEM_DIRECTORY="${CANDIDATE_SRC_DIRECTORY}/src/main/java/P${PROBLEM_ID}"

CANDIDATE_TEST_DIRECTORY="${CANDIDATE_SRC_DIRECTORY}/src/test/java/P${PROBLEM_ID}"

TARGET_CLASS_FILE_DIRECTORY="/home/egnikaiusr${CANDIDATE_SRC_DIRECTORY}/P${PROBLEM_ID}/target"

echo ${TARGET_CLASS_FILE_DIRECTORY}


rm -rf ${TARGET_CLASS_FILE_DIRECTORY};

echo "make directory ${TARGET_CLASS_FILE_DIRECTORY}"
mkdir -p ${TARGET_CLASS_FILE_DIRECTORY};


cd ${CANDIDATE_PROBLEM_DIRECTORY}
(>&2 echo "compiling code")
echo "compiling code"
javac -nowarn -d ${TARGET_CLASS_FILE_DIRECTORY} $(find . -name "*.java")

javac -nowarn -d ${TARGET_CLASS_FILE_DIRECTORY} -cp "${TARGET_CLASS_FILE_DIRECTORY}:/app/lib/*" $(find ${CANDIDATE_TEST_DIRECTORY} -name "*.java")

exit